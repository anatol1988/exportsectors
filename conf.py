# -*- coding: utf-8 -*-
"""
Configuration for exportsector script
"""

import datetime

#: Debug flag. If True - some debug information will be printed
DEBUG = True

#: Period of sending data for daemon
PERIOD = datetime.timedelta(minutes=0, seconds=10)

#: Command running from shell
COMMAND = 'starmaded smdo "export_sector'
