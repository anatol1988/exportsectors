#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
    exportsector
    ~~~~~~~~~~~~

    :author: Anatol Karalkou
    :contact: anatol1988@gmail.com
"""

import argparse
import csv
import time
import datetime
import subprocess

import conf


def print_debug(debug_str):
    """
    Prints string if debug flag is set
    """

    if conf.DEBUG:
        print(debug_str)


def call_command(sectors):
    """
    Calls predefined command with parameters

    Parameters
    __________
    sectors : list of sectors
    """

    cmd_str = '{0} {1} {2}.{3}"'.format(
              conf.COMMAND,
              ' '.join(sectors),
              datetime.date.today().strftime('%Y%m%d'),
              ''.join(sectors))

    print_debug(cmd_str)

    subprocess.call(cmd_str, shell=True)


def exportsectors(filename):
    """
    Passes sectors from file to daemon

    Parameters
    __________
    filename : sectors csv file name
    """

    with open(filename, 'r') as sector_file:
        reader = csv.reader(sector_file)

        for sectors in reader:
            call_command(sectors)
            time.sleep(conf.PERIOD.seconds)


def main():
    """
    Processing input parameters and calling chevron()
    """

    parser = argparse.ArgumentParser(
        description='Sends sectors from file to daemon')
    parser.add_argument('input', help='sectors file')
    args = parser.parse_args()
    exportsectors(args.input)

if __name__ == '__main__':
    main()
